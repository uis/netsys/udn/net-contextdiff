# ios.__init__


from .diff import CiscoIOSDiffConfig
from .config import CiscoIOSConfig


__all__ = [CiscoIOSDiffConfig, CiscoIOSConfig]